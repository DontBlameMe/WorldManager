prefix: "<gradient:#00bf8f:#9f9b0f>WorldManager</gradient>"

command:
  command: "WorldManager"
  aliases: [ "wm", "worldm", "wmanager" ]
  permission: "worldManager.use"
  no_permission_message: "<color:#ff5555>You are not allowed to use this command!"

global_items:
  gui_back:
    amount: 1
    name: "<color:#ff595c>GUI Back"
    lore: [ "<white>Go to the previous Inventory" ]
    material: "BARRIER"
    enabled: true

ignored_world_folders: [ "plugins", "cache", "config", "libraries", "logs", "plugins", "versions" ]

world_messages:
  world_unloaded_kick: "<color:#ffff00>You have been moved to another world, because your current world has been unloaded"

main_inventory:
  name: "<gradient:#00bf8f:#9f9b0f>WorldManager</gradient>"
  size: 27
  placeholder:
    material: "LIME_STAINED_GLASS_PANE"
  create_world:
    permission: "worldmanager.create"
    slot: 11
    amount: 1
    name: "<gradient:#00bf8f:#9f9b0f>Create World</gradient>"
    lore: [ "<white>Click to create a world" ]
    material: "GRASS_BLOCK"
  reload_configuration:
    permission: "worldmanager.reload"
    slot: 13
    amount: 1
    name: "<color:#ff5555>Reload config"
    lore: [ "<white>Reload the configuration" ]
    material: "WRITABLE_BOOK"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully reloaded the configuration file in %timeMillisms</gradient>"
  manage_worlds:
    permission: "worldmanager.manage"
    slot: 15
    amount: 1
    name: "<#f1fa8c>Manage Worlds"
    lore: [ "<white>Manage worlds" ]
    material: "COMPARATOR"

manage_worlds_inventory:
  name: "<gradient:#bd93f9:#8be9fd>Manage Worlds</gradient>"
  size: 27
  placeholder:
    material: "PURPLE_STAINED_GLASS_PANE"
  world:
    material: "GRASS_BLOCK"
    name: "<color:#ff79c6>%worldName"
    lore: [ "<white>Loaded: %loadedState" ]
    amount: 1
    loaded: "<color:#50fa7b>Yes"
    unloaded: "<color:#ff5555>No"
  next_page:
    slot: 24
    amount: 1
    name: "Next Page"
    lore: [ "<white>Click to go to the next page" ]
    material: "ARROW"
  previous_page:
    slot: 22
    amount: 1
    name: "Previous Page"
    lore: [ "<white>Click to go to the previous page" ]
    material: "ARROW"

manage_world_inventory:
  name: "<color:#ff79c6>%worldName"
  size: 27
  placeholder:
    material: "MAGENTA_STAINED_GLASS_PANE"
  delete:
    permission: "worldmanager.delete"
    slot: 10
    amount: 1
    name: "<color:#ff5555>Delete World"
    lore: [ "<white>Deletes the world" ]
    material: "HOPPER"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully deleted the world %worldName in %timeMillisms</gradient>"
  unload:
    permission: "worldmanager.unload"
    slot: 16
    amount: 1
    name: "<color:#6272a4>Unload"
    lore: [ "<white>Unloads the world" ]
    material: "REDSTONE_LAMP"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully unloaded %worldName in %timeMillisms</gradient>"
  load:
    permission: "worldmanager.load"
    slot: 16
    amount: 1
    name: "<color:#8be9fd>Load"
    lore: [ "<white>Loads the world" ]
    material: "LIGHT"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully loaded %worldName in %timeMillisms</gradient>"
  information:
    permission: "worldmanager.information"
    slot: 14
    amount: 1
    name: "<color:#50fa7b>Information"
    lore: [ "<color:#4be872>Loaded Chunks: %loadedChunks", "<color:#44cf67>Difficulty: %difficulty", "<color:#3ab057>Environment: %environment", "<color:#31944a>Seed: %seed" ]
    material: "PAPER"
  teleport:
    permission: "worldmanager.teleport"
    slot: 12
    amount: 1
    name: "<color:#bd93f9>Teleport"
    lore: [ "<white>Teleports you" ]
    material: "ENDER_PEARL"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully teleported you to %worldName</gradient>"

world_creation:
  type_selection:
    name: "<color:#0066EB>Select type"
    size: 27
    placeholder:
      material: "BLUE_STAINED_GLASS_PANE"
    flat:
      permission: "worldmanager.flatworld"
      slot: 11
      amount: 1
      name: "<color:#66EB00>Flatworld"
      lore: [ "<white>Click to create a flat world" ]
      material: "GRASS_BLOCK"
    normal:
      permission: "worldmanager.normalworld"
      slot: 13
      amount: 1
      name: "<color:#2AE883>Normal"
      lore: [ "<white>Click to create a normal world" ]
      material: "OAK_LOG"
    void:
      permission: "worldmanager.voidworld"
      slot: 15
      amount: 1
      name: "<gradient:#696969:#ffffff>Empty World</gradient>"
      lore: [ "<white>Click to create a empty world" ]
      material: "BEDROCK"
  environment_selection:
    name: "<color:#8A590A>Select Environment"
    size: 27
    placeholder:
      material: "BROWN_STAINED_GLASS_PANE"
    nether:
      permission: "worldmanager.nether"
      slot: 11
      amount: 1
      name: "<color:#910000>Nether"
      lore: [ "<white>Click to use the nether dimension" ]
      material: "NETHERRACK"
    overworld:
      permission: "worldmanager.overworld"
      slot: 13
      amount: 1
      name: "<color:#1C9903>Overworld"
      lore: [ "<white>Click to use the overworld dimension" ]
      material: "GRASS_BLOCK"
    end:
      permission: "worldmanager.end"
      slot: 15
      amount: 1
      name: "<color:#F2F061>End"
      lore: [ "<white>Click to use the end dimension" ]
      material: "END_STONE"
  world_name_selection:
    message: "<gradient:#ffffff:#E8C4FF>Please type the new world name in chat</gradient>"
    invalid_symbol_message: "<color:#ff5555>Please remove any special symbols from your message"
  seed_selection:
    message: "<gradient:#ffffff:#E8C4FF>Please say the seed for your new world in chat. If you use anything else than numbers, a random seed will be used</gradient>"
  success_message: "<gradient:#ffffff:#E8C4FF>The world %worldName has been successfully created and you have been teleported. This took %timeMillisms</gradient>"
  load:
    permission: "worldmanager.load"
    slot: 16
    amount: 1
    name: "<color:#8be9fd>Load"
    lore: [ "<white>Loads the world" ]
    material: "LIGHT"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully loaded %worldName in %timeMillisms</gradient>"
  information:
    permission: "worldmanager.information"
    slot: 14
    amount: 1
    name: "<color:#50fa7b>Information"
    lore: [ "<color:#4be872>Loaded Chunks: %loadedChunks", "<color:#44cf67>Difficulty: %difficulty", "<color:#3ab057>Environment: %environment", "<color:#31944a>Seed: %seed" ]
    material: "PAPER"
  teleport:
    permission: "worldmanager.teleport"
    slot: 12
    amount: 1
    name: "<color:#bd93f9>Teleport"
    lore: [ "<white>Teleports you" ]
    material: "ENDER_PEARL"
    success_message: "<gradient:#ffffff:#E8C4FF>Successfully teleported you to %worldName</gradient>"