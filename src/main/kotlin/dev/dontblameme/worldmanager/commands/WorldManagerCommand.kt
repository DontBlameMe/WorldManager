package dev.dontblameme.worldmanager.commands

import dev.dontblameme.kutilsapi.customcommand.CustomCommand
import dev.dontblameme.worldmanager.inventories.MainGUI
import dev.dontblameme.worldmanager.main.WorldManager

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class WorldManagerCommand {

    fun register() {
        command()
    }

    private fun command() {
        val customConfig = WorldManager.customConfig!!

        CustomCommand(
            commandName = customConfig.getValue("command", "command")!!,
            permission = customConfig.getValue("permission", "command")!!,
            permissionMessage = customConfig.getValue("no_permission_message", "command")!!,
            aliases = customConfig.getList("aliases", "command")!! as List<String>
        ) {
            MainGUI().open(it.player)
        }
    }
}