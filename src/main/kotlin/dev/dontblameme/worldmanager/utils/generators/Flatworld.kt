package dev.dontblameme.worldmanager.utils.generators

import org.bukkit.Material
import org.bukkit.World
import org.bukkit.generator.ChunkGenerator
import org.bukkit.generator.WorldInfo
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class Flatworld: ChunkGenerator() {

    override fun shouldGenerateCaves(): Boolean = false
    override fun shouldGenerateNoise(): Boolean = false
    override fun shouldGenerateStructures(): Boolean = false
    override fun shouldGenerateDecorations(): Boolean = false

    override fun generateSurface(worldInfo: WorldInfo, random: Random, chunkX: Int, chunkZ: Int, chunkData: ChunkData) {
        var topMaterial = Material.STONE
        val bottomMaterial = Material.BEDROCK
        var fillerMaterial = Material.STONE
        var maxHeight = -61

        when(worldInfo.environment) {
            World.Environment.NORMAL, World.Environment.CUSTOM -> {
                topMaterial = Material.GRASS_BLOCK
                fillerMaterial = Material.DIRT
            }
            World.Environment.NETHER -> {
                topMaterial = Material.NETHERRACK
                fillerMaterial= Material.NETHERRACK
                maxHeight = 61
            }
            World.Environment.THE_END -> {
                topMaterial = Material.END_STONE
                fillerMaterial = Material.END_STONE
                maxHeight = 61
            }
        }

        for(x in 0 until 16)
            for(z in 0 until 16)
                for(y in maxHeight downTo maxHeight -3)
                    when(y) {
                        maxHeight -> {
                            chunkData.setBlock(x, y, z, topMaterial)
                        }
                        maxHeight -3 -> chunkData.setBlock(x, y, z, bottomMaterial)
                        else -> chunkData.setBlock(x, y, z, fillerMaterial)
                    }
    }

}