package dev.dontblameme.worldmanager.utils

import dev.dontblameme.kutilsapi.textparser.TextParser.Companion.parseColors
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.generators.Flatworld
import dev.dontblameme.worldmanager.utils.generators.VoidWorld
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.WorldCreator
import java.io.File

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class WorldUtils private constructor() {
    companion object {
        fun isWorldLoaded(name: String): Boolean = Bukkit.getWorld(name) != null

        fun loadWorld(name: String): Boolean {
            for(worldFolder in Bukkit.getWorldContainer().listFiles()!!) {
               if(!worldFolder.name.equals(name, true) || !worldFolder.isDirectory || worldFolder.isFile)
                   continue

                Bukkit.createWorld(WorldCreator(name))
                return true
            }

            return false
        }

        fun unloadWorld(name: String): Boolean {
            if(!isWorldLoaded(name)) return false

            val world = Bukkit.getWorld(name)!!

            world.keepSpawnInMemory = false
            world.isAutoSave = false

            for(player in world.players) {
                player.sendMessage("${WorldManager.customConfig!!.getValue("prefix")} ${WorldManager.customConfig!!.getValue("world_unloaded_kick", "world_messages")}".parseColors())
                player.teleport(Bukkit.getWorlds()[0].spawnLocation)
            }

            for(loadedChunk in world.loadedChunks)
                loadedChunk.unload(true)

            Bukkit.unloadWorld(world, true)
            return true
        }

        fun deleteWorld(name: String): Boolean {
            if(!isWorldLoaded(name)) return false

            val world = Bukkit.getWorld(name)

            unloadWorld(name)

            deleteFle(world!!.worldFolder)

            return true
        }

        fun generateWorld(name: String, environment: World.Environment = World.Environment.NORMAL, type: CustomWorldType? = null, seed: Long? = null): World? {
            val worldCreator = WorldCreator(name)

            if(seed != null)
                worldCreator.seed(seed)

            if(type != null)
                when(type) {
                    CustomWorldType.FLAT -> worldCreator.generator(Flatworld())
                    CustomWorldType.VOID -> worldCreator.generator(VoidWorld())
                }

            worldCreator.environment(environment)

            return Bukkit.createWorld(worldCreator)
        }

        fun loadWorlds() {
            for((k,v) in getWorlds())
                if(!v)
                    Bukkit.createWorld(WorldCreator(k.name))
        }

        fun getWorlds(): HashMap<File, Boolean> {
            val ignoredFolders = WorldManager.customConfig!!.getList("ignored_world_folders")!!
            val list = HashMap<File, Boolean>()

            for(worldFolder in Bukkit.getWorldContainer().listFiles()!!)
                if(worldFolder.isDirectory && !worldFolder.isFile && worldFolder.listFiles()?.isNotEmpty() == true && !ignoredFolders.contains(worldFolder.name))
                    list[worldFolder] = isWorldLoaded(worldFolder.name)

            return list
        }

        private fun deleteFle(file: File) {
            if(file.isDirectory)
                for(f in file.listFiles()!!)
                    deleteFle(f)

            require(file.delete()) { "Error deleting file ${file.name}! This should not have happened! Please contact a developer and provide the following error code: WorldUtils.deleteFile failed" }
        }

    }

    enum class CustomWorldType {
        FLAT,
        VOID
    }
}