package dev.dontblameme.worldmanager.utils

import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.worldmanager.main.WorldManager
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class ItemGenerator private constructor() {
    companion object {
        fun generateItem(vararg section: String): ItemStack {
            val customConfig = WorldManager.customConfig!!
            val item = ItemBuilder(
                material = Material.getMaterial(customConfig.getValue("material", *section)!!)!!,
                amount = customConfig.getValue("amount", *section)!!.toInt(),
                displayName = customConfig.getValue("name", *section)!!,
                lores = LinkedList(customConfig.getList("lore", *section)!! as ArrayList<String>))

            return item.build()
        }
    }
}