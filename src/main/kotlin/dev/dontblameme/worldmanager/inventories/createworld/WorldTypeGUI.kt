package dev.dontblameme.worldmanager.inventories.createworld

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.worldmanager.inventories.MainGUI
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.ItemGenerator
import dev.dontblameme.worldmanager.utils.WorldUtils
import org.bukkit.Material
import org.bukkit.entity.Player
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 25, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class WorldTypeGUI {

    private val customConfig = WorldManager.customConfig!!
    private val flat = ItemGenerator.generateItem("world_creation", "type_selection", "flat")
    private val normal = ItemGenerator.generateItem("world_creation", "type_selection", "normal")
    private val void = ItemGenerator.generateItem("world_creation", "type_selection", "void")
    private val placeholder = ItemBuilder(
        amount = 1,
        displayName = "<red>",
        material = Material.getMaterial(customConfig.getValue("material", "world_creation", "type_selection", "placeholder")!!)!!
    )

    fun open(player: Player) {
        val inventory = InventoryBuilder(
            customConfig.getValue("name", "world_creation", "type_selection")!!,
            customConfig.getValue("size", "world_creation", "type_selection")!!.toInt())

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "type_selection", "flat")!!))
            inventory.addItem(flat, customConfig.getValue("slot", "world_creation", "type_selection", "flat")!!.toInt()) {
                WorldEnvironmentGUI().open(player, WorldUtils.CustomWorldType.FLAT)
            }

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "type_selection", "normal")!!))
            inventory.addItem(normal, customConfig.getValue("slot", "world_creation", "type_selection", "normal")!!.toInt()) {
                WorldEnvironmentGUI().open(player, null)
            }

        if(player.hasPermission(customConfig.getValue("permission", "world_creation", "type_selection", "void")!!))
            inventory.addItem(void, customConfig.getValue("slot", "world_creation", "type_selection", "void")!!.toInt()) {
                WorldEnvironmentGUI().open(player, WorldUtils.CustomWorldType.VOID)
            }

        if(customConfig.getValue("enabled", "global_items", "gui_back")!!.toBoolean())
            inventory.addItem(
                ItemBuilder(
                    material = Material.getMaterial(customConfig.getValue("material", "global_items", "gui_back")!!)!!,
                    displayName = customConfig.getValue("name", "global_items", "gui_back")!!,
                    amount = customConfig.getValue("amount", "global_items", "gui_back")!!.toInt(),
                    lores = LinkedList(customConfig.getList("lore", "global_items", "gui_back")!! as ArrayList<String>)
                ).build(),
                inventory.getSize() -1
            ) {
                MainGUI().open(player)
            }

        inventory.placeholder(placeholder.build())

        player.openInventory(inventory.build())
    }
}