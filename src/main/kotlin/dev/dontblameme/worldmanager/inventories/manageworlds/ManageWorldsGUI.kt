package dev.dontblameme.worldmanager.inventories.manageworlds

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.inventory.InventoryItem
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.kutilsapi.multipageinv.MultiPageButton
import dev.dontblameme.kutilsapi.multipageinv.MultiPageInventory
import dev.dontblameme.worldmanager.inventories.MainGUI
import dev.dontblameme.worldmanager.inventories.manageworlds.world.ManageWorldGUI
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.ItemGenerator
import dev.dontblameme.worldmanager.utils.WorldUtils
import org.bukkit.Material
import org.bukkit.entity.Player
import java.io.File
import java.util.*

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class ManageWorldsGUI {

    private val customConfig = WorldManager.customConfig!!
    private val previousPage = MultiPageButton(
        InventoryItem(ItemGenerator.generateItem("manage_worlds_inventory", "previous_page"), customConfig.getValue("slot", "manage_worlds_inventory", "previous_page")!!.toInt()), MultiPageButton.ButtonType.PREVIOUS)
    private val nextPage = MultiPageButton(
        InventoryItem(ItemGenerator.generateItem("manage_worlds_inventory", "next_page"), customConfig.getValue("slot", "manage_worlds_inventory", "next_page")!!.toInt()), MultiPageButton.ButtonType.NEXT)
    private val placeholder = ItemBuilder(
        amount = 1,
        displayName = "<red>",
        material = Material.getMaterial(customConfig.getValue("material", "manage_worlds_inventory", "placeholder")!!)!!
    )

    fun open(player: Player) {
        val multiPageInventory = MultiPageInventory()
        val worlds = WorldUtils.getWorlds()
        val invSize = customConfig.getValue("size", "manage_worlds_inventory")!!.toInt()
        var pages = worlds.size / invSize

        pages++

        while(pages >= 1) {
            var count = 0
            val inventory = InventoryBuilder(
                customConfig.getValue("name", "manage_worlds_inventory")!!,
                customConfig.getValue("size", "manage_worlds_inventory")!!.toInt())

            inventory.placeholder(placeholder.build())

            if(customConfig.getValue("enabled", "global_items", "gui_back")!!.toBoolean())
                inventory.addItem(
                    ItemBuilder(
                        material = Material.getMaterial(customConfig.getValue("material", "global_items", "gui_back")!!)!!,
                        displayName = customConfig.getValue("name", "global_items", "gui_back")!!,
                        amount = customConfig.getValue("amount", "global_items", "gui_back")!!.toInt(),
                        lores = LinkedList(customConfig.getList("lore", "global_items", "gui_back")!! as ArrayList<String>)
                    ).build(),
                    inventory.getSize() -1
                ) {
                    MainGUI().open(player)
                }

            val used = LinkedList<File>()

            for((worldFolder, isLoaded) in worlds) {
                if(count >= 54)
                    break

                if((count == nextPage.item.slot && pages == 1) ||
                    (count == previousPage.item.slot && pages == worlds.size / invSize) ||
                    count == invSize) {
                    count++
                    continue
                }

                val lore = customConfig.getList("lore", "manage_worlds_inventory", "world")!! as ArrayList<String>
                val list = lore.clone() as ArrayList<String>

                list.replaceAll {
                    it.replace(
                        "%loadedState",
                        customConfig.getValue(if(isLoaded) "loaded" else "unloaded", "manage_worlds_inventory", "world")!!
                    )
                }

                val worldItem = ItemBuilder(
                    displayName = customConfig.getValue("name", "manage_worlds_inventory", "world")!!.replace("%worldName", worldFolder.name),
                    amount = customConfig.getValue("amount", "manage_worlds_inventory", "world")!!.toInt(),
                    material = Material.getMaterial(customConfig.getValue("material", "manage_worlds_inventory", "world")!!)!!,
                    lores = LinkedList(list))

                inventory.addItem(worldItem.build(), count) {
                    ManageWorldGUI().open(it.whoClicked, worldFolder)
                }

                used.add(worldFolder)
                count++
            }

            for(folder in used)
                worlds.remove(folder)

            multiPageInventory.addGlobalButton(nextPage)
            multiPageInventory.addGlobalButton(previousPage)
            multiPageInventory.addPages(inventory)
            pages--
        }


        player.openInventory(multiPageInventory.build())
    }

}