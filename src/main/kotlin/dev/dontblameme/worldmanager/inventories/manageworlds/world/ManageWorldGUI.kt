package dev.dontblameme.worldmanager.inventories.manageworlds.world

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.kutilsapi.textparser.TextParser.Companion.parseColors
import dev.dontblameme.worldmanager.inventories.manageworlds.ManageWorldsGUI
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.ItemGenerator
import dev.dontblameme.worldmanager.utils.WorldUtils
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.HumanEntity
import org.bukkit.entity.Player
import java.io.File
import java.util.*
import kotlin.system.measureTimeMillis

/**
 * Creator: DontBlameMe69
 * Date: Mar 25, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class ManageWorldGUI {

    private val customConfig = WorldManager.customConfig!!
    private val delete = ItemGenerator.generateItem("manage_world_inventory", "delete")
    private val unload = ItemGenerator.generateItem("manage_world_inventory", "unload")
    private val load = ItemGenerator.generateItem("manage_world_inventory", "load")
    private val teleport = ItemGenerator.generateItem("manage_world_inventory", "teleport")
    private val placeholder = ItemBuilder(
        amount = 1,
        displayName = "<red>",
        material = Material.getMaterial(customConfig.getValue("material", "manage_world_inventory", "placeholder")!!)!!
    )

    fun open(player: HumanEntity, world: File) {
        val inventory = InventoryBuilder(
            customConfig.getValue("name", "manage_world_inventory")!!.replace("%worldName", world.name),
            customConfig.getValue("size", "manage_world_inventory")!!.toInt())

        if(player.hasPermission(customConfig.getValue("permission", "manage_world_inventory", "delete")!!))
            inventory.addItem(delete, customConfig.getValue("slot", "manage_world_inventory", "delete")!!.toInt()) {
                val timeMillis = measureTimeMillis {
                    WorldUtils.deleteWorld(world.name)
                }
                it.whoClicked.sendMessage("${customConfig.getValue("prefix")} ${customConfig.getValue("success_message", "manage_world_inventory", "delete")!!.replace("%worldName", world.name).replace("%timeMillis", "$timeMillis")}".parseColors())
                it.whoClicked.closeInventory()
            }

        val isLoaded = WorldUtils.isWorldLoaded(world.name)

        if(isLoaded) {
            val lore = customConfig.getList("lore", "manage_world_inventory", "information")!! as ArrayList<String>
            val world = Bukkit.getWorld(world.name)!!

            lore.replaceAll {it.replace("%environment", world.environment.name)}
            lore.replaceAll {it.replace("%seed", world.seed.toString())}
            lore.replaceAll {it.replace("%difficulty", world.difficulty.name)}
            lore.replaceAll {it.replace("%loadedChunks", world.loadedChunks.size.toString())}

            val information = ItemBuilder(
                displayName = customConfig.getValue("name", "manage_world_inventory", "information")!!,
                amount = customConfig.getValue("amount", "manage_world_inventory", "information")!!.toInt(),
                material = Material.getMaterial(customConfig.getValue("material", "manage_world_inventory", "information")!!)!!,
                lores = LinkedList(lore))

            if(player.hasPermission(customConfig.getValue("permission", "manage_world_inventory", "information")!!))
                inventory.addItem(information.build(), customConfig.getValue("slot", "manage_world_inventory", "information")!!.toInt())
        }

        if(isLoaded && player.hasPermission(customConfig.getValue("permission", "manage_world_inventory", "teleport")!!))
            inventory.addItem(teleport, customConfig.getValue("slot", "manage_world_inventory", "teleport")!!.toInt()) {
                it.whoClicked.teleport(Bukkit.getWorld(world.name)!!.spawnLocation)
                it.whoClicked.sendMessage("${customConfig.getValue("prefix")} ${customConfig.getValue("success_message", "manage_world_inventory", "teleport")!!.replace("%worldName", world.name)}".parseColors())
                it.whoClicked.closeInventory()
            }

        if(player.hasPermission(customConfig.getValue("permission", "manage_world_inventory", if(isLoaded) "unload" else "load")!!))
            inventory.addItem(if(isLoaded) unload else load, customConfig.getValue("slot", "manage_world_inventory", if(isLoaded) "unload" else "load")!!.toInt()) {
                val timeMillis = measureTimeMillis {
                    if (isLoaded)
                        WorldUtils.unloadWorld(world.name)
                    else
                        WorldUtils.loadWorld(world.name)
                }

                it.whoClicked.sendMessage("${customConfig.getValue("prefix")} ${customConfig.getValue("success_message", "manage_world_inventory", if(isLoaded) "unload" else "load")!!.replace("%worldName", world.name).replace("%timeMillis", "$timeMillis")}".parseColors())
                it.whoClicked.closeInventory()
            }

        if(customConfig.getValue("enabled", "global_items", "gui_back")!!.toBoolean())
            inventory.addItem(
                ItemBuilder(
                    material = Material.getMaterial(customConfig.getValue("material", "global_items", "gui_back")!!)!!,
                    displayName = customConfig.getValue("name", "global_items", "gui_back")!!,
                    amount = customConfig.getValue("amount", "global_items", "gui_back")!!.toInt(),
                    lores = LinkedList(customConfig.getList("lore", "global_items", "gui_back")!! as ArrayList<String>)
                ).build(),
                inventory.getSize() -1
            ) {
                ManageWorldsGUI().open(player as Player)
            }

        inventory.placeholder(placeholder.build())

        player.openInventory(inventory.build())
    }
}