package dev.dontblameme.worldmanager.inventories

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder
import dev.dontblameme.kutilsapi.itembuilder.ItemBuilder
import dev.dontblameme.kutilsapi.textparser.TextParser.Companion.parseColors
import dev.dontblameme.worldmanager.inventories.createworld.WorldTypeGUI
import dev.dontblameme.worldmanager.inventories.manageworlds.ManageWorldsGUI
import dev.dontblameme.worldmanager.main.WorldManager
import dev.dontblameme.worldmanager.utils.ItemGenerator
import org.bukkit.Material
import org.bukkit.entity.Player
import java.util.*
import kotlin.system.measureTimeMillis

/**
 * Creator: DontBlameMe69
 * Date: Mar 24, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class MainGUI {

    private val customConfig = WorldManager.customConfig!!
    private val createWorld = ItemGenerator.generateItem("main_inventory", "create_world")
    private val reloadConfig = ItemGenerator.generateItem("main_inventory", "reload_configuration")
    private val manageWorlds = ItemGenerator.generateItem("main_inventory", "manage_worlds")
    private val placeholder = ItemBuilder(
        amount = 1,
        displayName = "<red>",
        material = Material.getMaterial(customConfig.getValue("material", "main_inventory", "placeholder")!!)!!
    )

    fun open(player: Player) {
        val inventory = InventoryBuilder(
            customConfig.getValue("name", "main_inventory")!!,
            customConfig.getValue("size", "main_inventory")!!.toInt())

        if(player.hasPermission(customConfig.getValue("permission", "main_inventory", "create_world")!!))
            inventory.addItem(createWorld, customConfig.getValue("slot", "main_inventory", "create_world")!!.toInt()) {
                WorldTypeGUI().open(player)
            }

        if(player.hasPermission(customConfig.getValue("permission", "main_inventory", "reload_configuration")!!))
            inventory.addItem(reloadConfig, customConfig.getValue("slot", "main_inventory", "reload_configuration")!!.toInt()) {
                val timeMillis = measureTimeMillis {
                    customConfig.refresh()
                }
                player.closeInventory()
                player.sendMessage("${customConfig.getValue("prefix")!!} ${customConfig.getValue("success_message", "main_inventory", "reload_configuration")!!.replace("%timeMillis", "$timeMillis")}".parseColors())
            }

        if(player.hasPermission(customConfig.getValue("permission", "main_inventory", "manage_worlds")!!))
            inventory.addItem(manageWorlds, customConfig.getValue("slot", "main_inventory", "manage_worlds")!!.toInt()) {
                ManageWorldsGUI().open(player)
            }

        if(customConfig.getValue("enabled", "global_items", "gui_back")!!.toBoolean())
            inventory.addItem(
                ItemBuilder(
                    material = Material.getMaterial(customConfig.getValue("material", "global_items", "gui_back")!!)!!,
                    displayName = customConfig.getValue("name", "global_items", "gui_back")!!,
                    amount = customConfig.getValue("amount", "global_items", "gui_back")!!.toInt(),
                    lores = LinkedList(customConfig.getList("lore", "global_items", "gui_back")!! as ArrayList<String>)
                ).build(),
                inventory.getSize() -1
            ) {
                player.closeInventory()
            }

        inventory.placeholder(placeholder.build())

        player.openInventory(inventory.build())
    }

}