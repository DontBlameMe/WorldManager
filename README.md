<a href="https://nogithub.codeberg.page"><img src="https://nogithub.codeberg.page/badge.svg" alt="Please don't upload to GitHub"></a>
# Usage

To use this plugin simply download it and put it in your plugin folder.
Then you can use the /WorldManager command to access it.

**Important:**
This plugin needs the [KUtilsAPI](https://codeberg.org/DontBlameMe69/KUtilsAPI) installed to function.
This is an API I developed.

**Customizing:**
You can customize literally everything in the config.yml which can be found in *plugins/WorldManager/config.yml*
